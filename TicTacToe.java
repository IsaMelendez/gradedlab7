import java.util.Scanner;

public class TicTacToe
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);

		System.out.println("Welcome to Tic Tac Toe!");
		System.out.println("Player 1 is X, and player 2 is O.")

		boolean playGame = true;

		int winsP1 = 0;
		int winsP2 = 0;

		while(playGame)
		{
			Board myBoard = new Board();

			boolean gameOver = false;

			int player = 1;

			Square playerToken = Square.X;

			while(!gameOver)
			{
				System.out.println(myBoard);

				if(player==1)
				{
					playerToken = Square.X;
				}
				else
				{
					playerToken = Square.O;
				}

				System.out.println("What row will player " + player + " place a token on?");
				int row = sc.nextInt();
				row--;

				System.out.println("What column will player " + player + " place a token on?");
				int column = sc.nextInt();
				column--;

				while(!myBoard.placeToken(row, column, playerToken))
				{
					System.out.println("You cannot place a token here. Please input another position, making sure it's between 1 and 3.");

					System.out.println("What row will player " + player + " place a token on?");
					row = sc.nextInt();
					row--;

					System.out.println("What column will player " + player + " place a token on?");
					column = sc.nextInt();
					column--;
				}

				if(myBoard.checkIfFull())
				{
					System.out.println(myBoard);
					System.out.println("It's a tie! ");
					gameOver = true;
				}
				else if(myBoard.checkIfWinning(playerToken))
				{
					System.out.println(myBoard);
					System.out.println("Player " + player + " wins!");
					gameOver = true;

					if(player==1)
					{
						winsP1++;
					}
					else
					{
						winsP2++;
					}
				}
				else
				{
					player++;
					if(player>2)
					{
						player=1;
					}
				}
			}

			System.out.println("Would you like to play again?");
			String response = sc.nextLine();
			response = response.toLowerCase();

			while(!response.equals("yes") && !response.equals("no"))
			{
				System.out.println("Answer with yes or no.");
				response = sc.nextLine();
				response = response.toLowerCase();
			}
			
			if(response.equals("yes"))
			{
				playGame = true;
			}
			else
			{
				playGame = false;
				System.out.println("Player 1 has won " + winsP1 + " times!");
				System.out.println("Player 2 has won " + winsP2 + " times!");
				System.out.println("Thanks for playing!");
			}
		}
	}
}
