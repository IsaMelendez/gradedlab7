public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i<tictactoeBoard.length; i++)
		{
			for(int j = 0; j<tictactoeBoard[i].length; j++)
			{
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString()
	{
		String boardString = "";
		
		for(int i = 0; i<tictactoeBoard.length; i++)
		{
			String row = "";
			for(int j = 0; j<tictactoeBoard[i].length; j++)
			{
				row += tictactoeBoard[i][j] + " ";
			}
			boardString += row + "\n";
		}
		return boardString;
	}
	
	public boolean placeToken(int row, int col, Square playerToken)
	{
		if(row>=tictactoeBoard.length || row<0)
		{
			System.out.println("The value for the row is out of the given range for the board.");
			return false;
		}
		else if(col>=tictactoeBoard.length || col<0)
		{
			System.out.println("The value for the column is out of the given range for the board.");
			return false;
		}
		else
		{
			if(tictactoeBoard[row][col]==Square.BLANK)
			{
				tictactoeBoard[row][col]=playerToken;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	public boolean checkIfFull()
	{
		boolean isFull = true;
		
		for(int i = 0; i<tictactoeBoard.length; i++)
		{
			for(int j = 0; j<tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j] == Square.BLANK)
				{
					isFull = false;
				}
			}
		}
		
		return isFull;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		boolean youWin = false;
		int countTokens = 0;
		
		for(int i = 0; i<tictactoeBoard.length; i++)
		{
			for(int j = 0; j<tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j] == playerToken)
				{
					countTokens++;
				}
			}
			if(countTokens!=3)
			{
				countTokens = 0;
			}
		}
		
		if(countTokens==3)
		{
			youWin = true;
		}
		
		return youWin;
	}
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		boolean youWin = false;
		int countTokens = 0;
		
		for(int i = 0; i<tictactoeBoard.length; i++)
		{
			for(int j = 0; j<tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[j][i] == playerToken)
				{
					countTokens++;
				}
			}
			if(countTokens!=3)
			{
				countTokens = 0;
			}
		}
		
		if(countTokens==3)
		{
			youWin = true;
		}
		
		return youWin;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken)
	{
		boolean youWin = false;

		if(tictactoeBoard[1][1] == playerToken)
		{
			if(tictactoeBoard[2][0] == playerToken && tictactoeBoard[0][2] == playerToken)
			{
				youWin = true;
			}
			else if(tictactoeBoard[0][0] == playerToken && tictactoeBoard[2][2] == playerToken)
			{
				youWin = true;
			}
		}
		
		return youWin;
	}

	public boolean checkIfWinning(Square playerToken)
	{
		boolean returnValue = false;

		if(checkIfWinningHorizontal(playerToken))
		{
			returnValue = true;
		}
		else if(checkIfWinningVertical(playerToken))
		{
			returnValue = true;
		}
		else if(checkIfWinningDiagonal(playerToken))
		{
			returnValue = true;
		}

		return returnValue;
	}
}
